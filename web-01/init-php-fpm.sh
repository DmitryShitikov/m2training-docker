#!/bin/bash

if [ "${1#-}" != "$1" ]; then
    set -- php-fpm "$@"
fi

ps auxw | grep php-fpm | grep -v grep > /dev/null
if [ $? != 0 ]; then
    echo "restarting php-fpm"
    /etc/init.d/php7.3-fpm restart
else
    echo "starting php-fpm"
    /etc/init.d/php7.3-fpm start
fi

update-alternatives --set php /usr/bin/php7.3